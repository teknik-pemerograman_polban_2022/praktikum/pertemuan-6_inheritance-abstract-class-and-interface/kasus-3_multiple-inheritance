interface Sortable {
    public abstract int compare(Sortable b);

    public abstract Sortable[] shell_sort(Sortable[] a);
}
